package AlmacenModel

import (
	"fmt"

	"../../Modelos/CatalogoModel"
	"../../Modelos/ConexionModel"
	"../../Modulos/Conexiones"
	"../../Modulos/Variables"
	"gopkg.in/mgo.v2/bson"
)

//IAlmacen interface con los métodos de la clase
type IAlmacen interface {
	InsertaMgo() bool
	InsertaElastic() bool

	ActualizaMgo(campos []string, valores []interface{}) bool
	ActualizaElastic(campos []string, valores []interface{}) bool //Reemplaza No Actualiza

	ReemplazaMgo() bool
	ReemplazaElastic() bool

	ConsultaExistenciaByFieldMgo(field string, valor string)

	ConsultaExistenciaByIDMgo() bool
	ConsultaExistenciaByIDElastic() bool

	EliminaByIDMgo() bool
	EliminaByIDElastic() bool
}

//################################################<<METODOS DE GESTION >>################################################################

//##################################<< INSERTAR >>###################################

//InsertaMgo es un método que crea un registro en Mongo
func (p AlmacenMgo) InsertaMgo() bool {
	result := false
	s, Almacens, err := MoConexion.GetColectionMgo(MoVar.ColeccionAlmacen)
	if err != nil {
		fmt.Println(err)
	}

	err = Almacens.Insert(p)
	if err != nil {
		fmt.Println(err)
	} else {
		result = true
	}

	s.Close()
	return result
}

//InsertaElastic es un método que crea un registro en Mongo
func (p AlmacenMgo) InsertaElastic() bool {
	AlmacenE := p.PreparaDatosELastic()
	insert := MoConexion.InsertaElastic(MoVar.TipoAlmacen, p.ID.Hex(), AlmacenE)
	if !insert {
		fmt.Println("Error al insertar Almacen en Elastic")
		return false
	}
	return true
}

//##########################<< UPDATE >>############################################

//ActualizaMgo es un método que encuentra y Actualiza un registro en Mongo
//IMPORTANTE --> Debe coincidir el número y orden de campos con el de valores
func (p AlmacenMgo) ActualizaMgo(campos []string, valores []interface{}) bool {
	result := false
	s, Almacens, err := MoConexion.GetColectionMgo(MoVar.ColeccionAlmacen)
	var Abson bson.M
	Abson = make(map[string]interface{})
	for k, v := range campos {
		Abson[v] = valores[k]
	}
	change := bson.M{"$set": Abson}
	err = Almacens.Update(bson.M{"_id": p.ID}, change)
	if err != nil {
		fmt.Println(err)
	} else {
		result = true
	}
	s.Close()
	return result
}

//ActualizaElastic es un método que encuentra y Actualiza un registro en Mongo
func (p AlmacenMgo) ActualizaElastic() error {
	AlmacenE := p.PreparaDatosELastic()
	err := MoConexion.ActualizaElastic(MoVar.ColeccionAlmacen, p.ID.Hex(), AlmacenE)
	return err
}

//##########################<< REEMPLAZA >>############################################

//ReemplazaMgo es un método que encuentra y Actualiza un registro en Mongo
func (p AlmacenMgo) ReemplazaMgo() bool {
	result := false
	s, Almacens, err := MoConexion.GetColectionMgo(MoVar.ColeccionAlmacen)
	err = Almacens.Update(bson.M{"_id": p.ID}, p)
	if err != nil {
		fmt.Println(err)
	} else {
		result = true
	}
	s.Close()
	return result
}

//ReemplazaElastic es un método que encuentra y reemplaza un Almacen en elastic
func (p AlmacenMgo) ReemplazaElastic() bool {
	delete := MoConexion.DeleteElastic(MoVar.TipoAlmacen, p.ID.Hex())
	if !delete {
		fmt.Println("Error al actualizar Almacen en Elastic")
		return false
	}
	insert := MoConexion.InsertaElastic(MoVar.TipoAlmacen, p.ID.Hex(), p)
	if !insert {
		fmt.Println("Error al actualizar Almacen en Elastic")
		return false
	}
	return true
}

//###########################<< CONSULTA EXISTENCIAS >>###################################

//ConsultaExistenciaByFieldMgo es un método que verifica si un registro existe en Mongo indicando un campo y un valor string
func (p AlmacenMgo) ConsultaExistenciaByFieldMgo(field string, valor string) bool {
	result := false
	s, Almacens, err := MoConexion.GetColectionMgo(MoVar.ColeccionAlmacen)
	if err != nil {
		fmt.Println(err)
	}
	n, e := Almacens.Find(bson.M{field: valor}).Count()
	if e != nil {
		fmt.Println(e)
	}
	if n > 0 {
		result = true
	}
	s.Close()
	return result
}

//ConsultaExistenciaByIDMgo es un método que encuentra un registro en Mongo buscándolo por ID
func (p AlmacenMgo) ConsultaExistenciaByIDMgo() bool {
	result := false
	s, Almacens, err := MoConexion.GetColectionMgo(MoVar.ColeccionAlmacen)
	if err != nil {
		fmt.Println(err)
	}
	n, e := Almacens.Find(bson.M{"_id": p.ID}).Count()
	if e != nil {
		fmt.Println(e)
	}
	if n > 0 {
		result = true
	}
	s.Close()
	return result
}

//ConsultaExistenciaByIDElastic es un método que encuentra un registro en Mongo buscándolo por ID
func (p AlmacenMgo) ConsultaExistenciaByIDElastic() bool {
	result := MoConexion.ConsultaElastic(MoVar.TipoAlmacen, p.ID.Hex())
	return result
}

//##################################<< ELIMINACIONES >>#################################################

//EliminaByIDMgo es un método que elimina un registro en Mongo
func (p AlmacenMgo) EliminaByIDMgo() bool {
	var result bool
	s, Almacens, err := MoConexion.GetColectionMgo(MoVar.ColeccionAlmacen)
	if err != nil {
		fmt.Println("Error al consultar la coleccion de almacenes", err)
	}
	e := Almacens.RemoveId(p.ID)
	if e != nil {
		fmt.Println("Error al eliminar el almacen en Mongdb: ", p.ID, e)
		result = false
	} else {
		result = true
	}
	s.Close()
	return result
}

//EliminaByIDElastic es un método que elimina un registro en Mongo
func (p AlmacenMgo) EliminaByIDElastic() bool {
	delete := MoConexion.DeleteElastic(MoVar.TipoAlmacen, p.ID.Hex())
	if !delete {
		fmt.Println("Error al actualizar Almacen en Elastic")
		return false
	}
	return true
}

//PreparaDatosELastic  obtiene los datos por defecto de mongo y los convierte en string de tal forma que
//se inserteadecuadamente en elastic
func (p AlmacenMgo) PreparaDatosELastic() AlmacenElastic {
	var AlmacenE AlmacenElastic
	AlmacenE.Nombre = p.Nombre
	AlmacenE.Tipo = CatalogoModel.RegresaNombreSubCatalogo(p.Tipo)
	AlmacenE.Clasificacion = CatalogoModel.RegresaNombreSubCatalogo(p.Clasificacion)
	if p.Predecesor != "" {
		AlmacenPredesesor := GetOne(p.Predecesor)
		AlmacenE.Predecesor = AlmacenPredesesor.Nombre
	}
	if p.Conexion != "" {
		parametrosAlmacenes := ConexionModel.GetOne(p.Conexion)
		AlmacenE.NombreConexion = parametrosAlmacenes.Nombre
	}
	AlmacenE.Estatus = CatalogoModel.RegresaNombreSubCatalogo(p.Estatus)
	AlmacenE.FechaHora = p.FechaHora
	return AlmacenE
}
