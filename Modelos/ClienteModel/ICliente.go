package ClienteModel

import (
	"fmt"

	"../../Modulos/Conexiones"
	"../../Modulos/Variables"
	"gopkg.in/mgo.v2/bson"
)

//ICliente interface con los métodos de la clase
type ICliente interface {
	InsertaMgo() bool
	InsertaElastic() bool

	ActualizaMgo(campos []string, valores []interface{}) bool
	ActualizaElastic(campos []string, valores []interface{}) bool //Reemplaza No Actualiza

	ReemplazaMgo() bool
	ReemplazaElastic() bool

	ConsultaExistenciaByFieldMgo(field string, valor string)

	ConsultaExistenciaByIDMgo() bool
	ConsultaExistenciaByIDElastic() bool

	EliminaByIDMgo() bool
	EliminaByIDElastic() bool
}

//################################################<<METODOS DE GESTION >>################################################################

//##################################<< INSERTAR >>###################################

//InsertaMgo es un método que crea un registro en Mongo
func (p ClienteMgo) InsertaMgo() bool {
	result := false
	s, Clientes, err := MoConexion.GetColectionMgo(MoVar.ColeccionCliente)
	if err != nil {
		fmt.Println(err)
	}

	err = Clientes.Insert(p)
	if err != nil {
		fmt.Println(err)
	} else {
		result = true
	}

	s.Close()
	return result
}

//InsertaElastic es un método que crea un registro en Mongo
func (p ClienteMgo) InsertaElastic() bool {
	var ClienteE ClienteElastic

	// ClienteE.IDPersona = p.IDPersona
	// ClienteE.RFC = p.RFC
	// ClienteE.Direcciones = p.Direcciones
	// ClienteE.MediosDeContacto = p.MediosDeContacto
	// ClienteE.PersonasContacto = p.PersonasContacto
	// ClienteE.Almacenes = p.Almacenes
	// ClienteE.Notificaciones = p.Notificaciones
	// ClienteE.Estatus = p.Estatus
	// ClienteE.FechaHora = p.FechaHora
	insert := MoConexion.InsertaElastic(MoVar.TipoCliente, p.ID.Hex(), ClienteE)
	if !insert {
		fmt.Println("Error al insertar Cliente en Elastic")
		return false
	}
	return true
}

//##########################<< UPDATE >>############################################

//ActualizaMgo es un método que encuentra y Actualiza un registro en Mongo
//IMPORTANTE --> Debe coincidir el número y orden de campos con el de valores
func (p ClienteMgo) ActualizaMgo(campos []string, valores []interface{}) bool {
	result := false
	s, Clientes, err := MoConexion.GetColectionMgo(MoVar.ColeccionCliente)
	var Abson bson.M
	Abson = make(map[string]interface{})
	for k, v := range campos {
		Abson[v] = valores[k]
	}
	change := bson.M{"$set": Abson}
	err = Clientes.Update(bson.M{"_id": p.ID}, change)
	if err != nil {
		fmt.Println(err)
	} else {
		result = true
	}
	s.Close()
	return result
}

//ActualizaElastic es un método que encuentra y Actualiza un registro en Mongo
func (p ClienteMgo) ActualizaElastic() bool {
	delete := MoConexion.DeleteElastic(MoVar.TipoCliente, p.ID.Hex())
	if !delete {
		fmt.Println("Error al actualizar Cliente en Elastic")
		return false
	}

	if !p.InsertaElastic() {
		fmt.Println("Error al actualizar Cliente en Elastic, se perdió Referencia.")
		return false
	}
	return true
}

//##########################<< REEMPLAZA >>############################################

//ReemplazaMgo es un método que encuentra y Actualiza un registro en Mongo
func (p ClienteMgo) ReemplazaMgo() bool {
	result := false
	s, Clientes, err := MoConexion.GetColectionMgo(MoVar.ColeccionCliente)
	err = Clientes.Update(bson.M{"_id": p.ID}, p)
	if err != nil {
		fmt.Println(err)
	} else {
		result = true
	}
	s.Close()
	return result
}

//ReemplazaElastic es un método que encuentra y reemplaza un Cliente en elastic
func (p ClienteMgo) ReemplazaElastic() bool {
	delete := MoConexion.DeleteElastic(MoVar.TipoCliente, p.ID.Hex())
	if !delete {
		fmt.Println("Error al actualizar Cliente en Elastic")
		return false
	}
	insert := MoConexion.InsertaElastic(MoVar.TipoCliente, p.ID.Hex(), p)
	if !insert {
		fmt.Println("Error al actualizar Cliente en Elastic")
		return false
	}
	return true
}

//###########################<< CONSULTA EXISTENCIAS >>###################################

//ConsultaExistenciaByFieldMgo es un método que verifica si un registro existe en Mongo indicando un campo y un valor string
func (p ClienteMgo) ConsultaExistenciaByFieldMgo(field string, valor string) bool {
	result := false
	s, Clientes, err := MoConexion.GetColectionMgo(MoVar.ColeccionCliente)
	if err != nil {
		fmt.Println(err)
	}
	n, e := Clientes.Find(bson.M{field: valor}).Count()
	if e != nil {
		fmt.Println(e)
	}
	if n > 0 {
		result = true
	}
	s.Close()
	return result
}

//ConsultaExistenciaByIDMgo es un método que encuentra un registro en Mongo buscándolo por ID
func (p ClienteMgo) ConsultaExistenciaByIDMgo() bool {
	result := false
	s, Clientes, err := MoConexion.GetColectionMgo(MoVar.ColeccionCliente)
	if err != nil {
		fmt.Println(err)
	}
	n, e := Clientes.Find(bson.M{"_id": p.ID}).Count()
	if e != nil {
		fmt.Println(e)
	}
	if n > 0 {
		result = true
	}
	s.Close()
	return result
}

//ConsultaExistenciaByIDElastic es un método que encuentra un registro en Mongo buscándolo por ID
func (p ClienteMgo) ConsultaExistenciaByIDElastic() bool {
	result := MoConexion.ConsultaElastic(MoVar.TipoCliente, p.ID.Hex())
	return result
}

//##################################<< ELIMINACIONES >>#################################################

//EliminaByIDMgo es un método que elimina un registro en Mongo
func (p ClienteMgo) EliminaByIDMgo() bool {
	result := false
	s, Clientes, err := MoConexion.GetColectionMgo(MoVar.ColeccionCliente)
	if err != nil {
		fmt.Println(err)
	}
	e := Clientes.RemoveId(bson.M{"_id": p.ID})
	if e != nil {
		result = true
	} else {
		fmt.Println(e)
	}
	s.Close()
	return result
}

//EliminaByIDElastic es un método que elimina un registro en Mongo
func (p ClienteMgo) EliminaByIDElastic() bool {
	delete := MoConexion.DeleteElastic(MoVar.TipoCliente, p.ID.Hex())
	if !delete {
		fmt.Println("Error al actualizar Cliente en Elastic")
		return false
	}
	return true
}
